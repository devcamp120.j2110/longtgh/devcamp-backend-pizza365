// Import thư viện Moogoose
const mongoose = require("mongoose");

// Sử dụng phép gán phá hủy cấu trúc đối tượng để lấy thuộc tính Schema của mongoose
const { Schema } = mongoose;

// Khởi tạo Order Schema MongoDB
const orderSchema = new Schema({
    _id: Schema.Types.ObjectId, // Trường _id có kiểu dữ liệu ObjectID
    OrderCode: {
        type: String,              
        unique: true,
        maxlength: 6,
        default: function() {
            return Math.random().toString(36).substring(2,8); // generate random string with length of 6
        } 
    },
    PizzaSize: {
        type: String,
        required: true,         
    },
    PizzaType: {
        type: String,
        require: true
    },
    Voucher: {
        type: String        
    },
    Status: {
        type: String,
        default: function(){
            let arr = ['Created', 'Confirmed','Canceled'];
            return arr[Math.floor(Math.random() * arr.length)];
        }
    },
    CreatedDate: {
        type: Date,
        default: Date.now
    }
})

//Tạo Order Model
const OrderModel = mongoose.model("Order", orderSchema);

//Export Order Model
module.exports = { OrderModel };