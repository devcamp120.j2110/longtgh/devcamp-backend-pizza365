// Import thư viện Moogoose
const mongoose = require("mongoose");

// Sử dụng phép gán phá hủy cấu trúc đối tượng để lấy thuộc tính Schema của mongoose
const { Schema } = mongoose;

// Khởi tạo User Schema MongoDB
const userSchema = new Schema({
    _id: Schema.Types.ObjectId, // Trường _id có kiểu dữ liệu ObjectID
    FullName: {
        type: String,
        required: true,        
    },
    Email: {
        type: String,
        required: true, 
        unique: true
    },
    Address: {
        type: String,
        require: true
    },
    Phone: {
        type: String,
        required: true
    },
    // Trường orders là một mảng ObjectID, mỗi một ID sẽ liên kết tới 1 order có _id tương ứng
    Orders: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Order'
        }
    ]

})

//Tạo User Model
const UserModel = mongoose.model("User", userSchema);

//Export User Model
module.exports = { UserModel };