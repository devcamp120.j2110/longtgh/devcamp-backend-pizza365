// Import thư viện Moogoose
const mongoose = require("mongoose");

// Sử dụng phép gán phá hủy cấu trúc đối tượng để lấy thuộc tính Schema của mongoose
const { Schema } = mongoose;

// Khởi tạo Voucher Schema MongoDB
const voucherSchema = new Schema({
    _id: Schema.Types.ObjectId, // Trường _id có kiểu dữ liệu ObjectID
    code: {
        type: String,
        required: true,
        unique: true
    },
    discountPercent: {
        type: Number,
        required: true
    }
})

//Tạo Voucher Model
const VoucherModel = mongoose.model("Voucher", voucherSchema);

//Export Voucher Model
module.exports = { VoucherModel };