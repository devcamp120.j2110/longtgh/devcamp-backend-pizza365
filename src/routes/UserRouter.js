const express = require("express");

const router = express.Router();

const { createUser, getAllUser, getUserDetail, updateUserByID, deleteUser } = require("../controllers/UserController");
const { createOrder, getAllOrderOfUser } = require("../controllers/OrderController");

router.post("/", createUser);
router.get("/", getAllUser);

router.get("/:userId", getUserDetail);
router.put("/:userId", updateUserByID);
router.delete("/:userId", deleteUser);

// /users/:userId/orders - Create Order
router.post('/:userId/orders', createOrder);
// /users/:userId/orders - Get all order of user
router.get('/:userId/orders', getAllOrderOfUser);

module.exports = router;
