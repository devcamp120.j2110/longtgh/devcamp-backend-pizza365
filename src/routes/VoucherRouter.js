const express = require("express");

const router = express.Router();

const { createVoucher, getAllVoucher, getVoucherDetail, updateVoucherByID, deleteVoucher } = require("../controllers/VoucherController");

router.post("/", createVoucher);
router.get("/", getAllVoucher);

router.get('/:voucherId', getVoucherDetail);
router.put('/:voucherId', updateVoucherByID);
router.delete('/:voucherId', deleteVoucher);

module.exports = router;