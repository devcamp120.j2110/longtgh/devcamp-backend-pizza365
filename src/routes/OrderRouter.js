// Import thư viện Express
const express = require('express');

// Import các hàm của lớp controller
const { getAllOrder, deleteOrder, getSingleOrder, updateOrder } = require("../controllers/OrderController");

// Khai báo exporess router
const router = express.Router();

// Khai báo các router dạng "/orders" + url bên dưới sẽ gọi đến các hàm tương ứng

// /orders - Get All Orders
router.get('/', getAllOrder);
// /orders/:orderId - Get Order By ID
router.get('/:orderId', getSingleOrder);
// /orders/:orderId - Update Order By ID
router.put('/:orderId', updateOrder);
// /orders/:orderId - Delete Order By ID
router.delete('/:orderId', deleteOrder);

// Export router dưới dạng module để sử dụng
module.exports = router;
