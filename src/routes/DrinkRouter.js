const express = require("express");

const router = express.Router();

const { createDrink, getAllDrink, getDrinkDetail, updateDrinkByID, deleteDrink } = require("../controllers/DrinkController");

router.post("/", createDrink);
router.get("/", getAllDrink);

router.get('/:drinkId', getDrinkDetail);
router.put('/:drinkId', updateDrinkByID);
router.delete('/:drinkId', deleteDrink);


module.exports = router;