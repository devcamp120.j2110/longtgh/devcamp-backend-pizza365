const mongoose = require("mongoose");

const { UserModel } = require("../models/UserModel");

function createUser (request, response) {
    const user = new UserModel({
        _id: mongoose.Types.ObjectId(),
        FullName: request.body.FullName,
        Email: request.body.Email,
        Address: request.body.Address,
        Phone: request.body.Phone
    });

    user.save()
        .then((newUser) => {
            return response.status(200).json({
                message: "Success",
                user: newUser
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function getAllUser (request, response) {
    UserModel.find()
        .select("_id FullName Email Address Phone Orders")
        .then((userList) => {
            return response.status(200).json({
                message: "Success",
                users: userList
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}
function getUserDetail(request, response) {
    // Lấy userId từ params URL
    const userId = request.params.userId;

    // Kiểm tra xem userId có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(userId)) {
        UserModel.findById(userId)
            .then((data) => {
                if (data) {
                    return response.status(200).json({
                        message: "Success",
                        user: data
                    })
                } else {
                    return response.status(404).json({
                        message: "Fail",
                        error: "Not found"
                    })
                }
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })

    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "UserID is not valid"
        })
    }
}

function updateUserByID(request, response) {
    // Lấy userId từ params URL
    const userId = request.params.userId;

    const updateObject = request.body;

    // Kiểm tra xem userId có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(userId)) {
        UserModel.findByIdAndUpdate(userId, updateObject)
            .then(() => {
                return response.status(200).json({
                    message: "Success",
                    updatedObject: updateObject
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "UserID is not valid"
        })
    }
}

function deleteUser(request, response) {
    // Lấy userId từ params URL
    const userId = request.params.userId;

    // Kiểm tra xem userId có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(userId)) {
        UserModel.findByIdAndDelete(userId)
            .then(() => {
                return response.status(200).json({
                    message: "Success"
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "UserID is not valid"
        })
    }
}

module.exports = { createUser, getAllUser, getUserDetail, updateUserByID, deleteUser }