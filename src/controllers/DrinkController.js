const mongoose = require("mongoose");

const { DrinkModel } = require("../models/DrinkModel");

function createDrink (request, response) {
    const drink = new DrinkModel({
        _id: mongoose.Types.ObjectId(),
        code: request.body.code,
        name: request.body.name
    });

    drink.save()
        .then((newDrink) => {
            return response.status(200).json({
                message: "Success",
                drink: newDrink
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function getAllDrink (request, response) {
    DrinkModel.find()
        .select("_id code name")
        .then((drinkList) => {
            return response.status(200).json({
                message: "Success",
                drinks: drinkList
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}
function getDrinkDetail(request, response) {
    // Lấy drinkId từ params URL
    const drinkId = request.params.drinkId;

    // Kiểm tra xem drinkId có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(drinkId)) {
        DrinkModel.findById(drinkId)
            .then((data) => {
                if (data) {
                    return response.status(200).json({
                        message: "Success",
                        drink: data
                    })
                } else {
                    return response.status(404).json({
                        message: "Fail",
                        error: "Not found"
                    })
                }
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "DrinkID is not valid"
        })
    }
}

function updateDrinkByID(request, response) {
    // Lấy drinkId từ params URL
    const drinkId = request.params.drinkId;

    const updateObject = request.body;

    // Kiểm tra xem drinkId có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(drinkId)) {
        DrinkModel.findByIdAndUpdate(drinkId, updateObject)
            .then((updatedObject) => {
                return response.status(200).json({
                    message: "Success",
                    updatedObject: updatedObject
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "DrinkID is not valid"
        })
    }
}

function deleteDrink(request, response) {
    // Lấy drinkId từ params URL
    const drinkId = request.params.drinkId;

    // Kiểm tra xem drinkId có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(drinkId)) {
        DrinkModel.findByIdAndDelete(drinkId)
            .then(() => {
                return response.status(204).json({
                    message: "Success"
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "DrinkID is not valid"
        })
    }
}

module.exports = { createDrink, getAllDrink, getDrinkDetail, updateDrinkByID, deleteDrink }