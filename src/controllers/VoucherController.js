const mongoose = require("mongoose");

const { VoucherModel } = require("../models/VoucherModel");

function createVoucher (request, response) {
    const voucher = new VoucherModel({
        _id: mongoose.Types.ObjectId(),
        code: request.body.code,
        discountPercent: request.body.discountPercent
    });

    voucher.save()
        .then((newVoucher) => {
            return response.status(200).json({
                message: "Success",
                voucher: newVoucher
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}

function getAllVoucher (request, response) {
    VoucherModel.find()
        .select("_id code discountPercent")
        .then((voucherList) => {
            return response.status(200).json({
                message: "Success",
                vouchers: voucherList
            })
        })
        .catch((error) => {
            return response.status(500).json({
                message: "Fail",
                error: error.message
            })
        })
}
function getVoucherDetail(request, response) {
    // Lấy voucherId từ params URL
    const voucherId = request.params.voucherId;

    // Kiểm tra xem voucherId có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(voucherId)) {
        VoucherModel.findById(voucherId)
            .then((data) => {
                if (data) {
                    return response.status(200).json({
                        message: "Success",
                        voucher: data
                    })
                } else {
                    return response.status(404).json({
                        message: "Fail",
                        error: "Not found"
                    })
                }
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })

    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "VoucherID is not valid"
        })
    }
}

function updateVoucherByID(request, response) {
    // Lấy voucherId từ params URL
    const voucherId = request.params.voucherId;

    const updateObject = request.body;

    // Kiểm tra xem voucherId có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(voucherId)) {
        VoucherModel.findByIdAndUpdate(voucherId, updateObject)
            .then((updatedObject) => {
                return response.status(200).json({
                    message: "Success",
                    updatedObject: updatedObject
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "VoucherID is not valid"
        })
    }
}

function deleteVoucher(request, response) {
    // Lấy voucherId từ params URL
    const voucherId = request.params.voucherId;

    // Kiểm tra xem voucherId có phải ObjectID hay không 
    if (mongoose.Types.ObjectId.isValid(voucherId)) {
        VoucherModel.findByIdAndDelete(voucherId)
            .then(() => {
                return response.status(200).json({
                    message: "Success"
                })
            })
            .catch((error) => {
                return response.status(500).json({
                    message: "Fail",
                    error: error.message
                })
            })
    } else {
        // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
        return response.status(400).json({
            message: "Fail",
            error: "VoucherID is not valid"
        })
    }
}

module.exports = { createVoucher, getAllVoucher, getVoucherDetail, updateVoucherByID, deleteVoucher }