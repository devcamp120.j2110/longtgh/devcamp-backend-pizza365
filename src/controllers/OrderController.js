// Import thư viện mongoose JS
const mongoose = require('mongoose');

// Import User Model
const { UserModel } = require('../models/UserModel');
// Import Order Model
const { OrderModel } = require('../models/OrderModel');

// Create Order
function createOrder(req, res) {
     // Khởi tạo một đối tượng OrderModel  mới truyền các tham số tương ứng từ request body vào
    const order = new OrderModel({
        // Thuộc tính _id - Random một ObjectID
        _id: mongoose.Types.ObjectId(),        
        PizzaSize: req.body.PizzaSize,
        PizzaType: req.body.PizzaType,
        Voucher: req.body.Voucher
    });

    // Gọi hàm order save - là 1 promise (Tiến trình bất đồng bộ)
    order.save()
        // Sau khi tạo order thành công
        .then(function(newOrder) {
            // Lấy userId từ params URL (Khác với Query URL (sau ?))
            var userId = req.params.userId;

            // Gọi hàm UserModel .findOneAndUpdate truyền vào điều kiện (_id = ?) và thêm _id của order mới tạo vào mảng orders
            return UserModel.findOneAndUpdate({ _id: userId }, {$push: {Orders: newOrder._id}}, { new: true });
        })
        // Sau khi update user thành công trả ra status 200 - Success
        .then((updatedUser) => {
            return res.status(200).json({
                success: true,
                message: 'New Order created successfully on User',
                User: updatedUser,
            });
        })
        // Xử lý lỗi trả ra 500 - Server Internal Error
        .catch((error) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: error.message,
            });
        });
}

// Get all order of user 
function getAllOrderOfUser(req, res) {
    // Lấy userId từ params URL (Khác với Query URL (sau ?))
    const userId = req.params.userId;

    // Gọi hàm .findById để tìm kiếm user dựa vào ID
    UserModel.findById(userId)
        // Lấy chi tiết Order dựa vào ánh xạ _id của từng phần tử trong trường orders
        .populate({path: 'Orders'})
        // Nếu thành công trả ra status 200 - Success
        .then((singleUser) => {
            res.status(200).json({
                success: true,
                message: `More orders on ${singleUser.FullName}`,
                Orders: singleUser
            });
        })
        // Xử lý lỗi trả ra 500 - Server Internal Error
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This user does not exist',
                error: err.message,
            });
        });
}

// Get all order
function getAllOrder(req, res) {
    OrderModel.find()
        .select('_id OrderCode PizzaSize PizzaType Voucher Status CreatedDate')
        .then((allOrder) => {
            return res.status(200).json({
                success: true,
                message: 'A list of all order',
                Order: allOrder,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.',
                error: err.message,
            });
        });
}

// Get single order
function getSingleOrder(req, res) {
    const id = req.params.orderId;

    OrderModel.findById(id)
        .then((singleOrder) => {
            res.status(200).json({
                success: true,
                message: `Get data on Order`,
                Order: singleOrder,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'This order does not exist',
                error: err.message,
            });
        });
}

// update order
function updateOrder(req, res) {
    const orderId = req.params.orderId;
    const updateObject = req.body;
    OrderModel.findByIdAndUpdate(orderId, updateObject)
        .then(() => {
            res.status(200).json({
                success: true,
                message: 'Order is updated',
                updateOrder: updateObject,
            });
        })
        .catch((err) => {
            res.status(500).json({
                success: false,
                message: 'Server error. Please try again.'
            });
        });
}

// delete a order
function deleteOrder(req, res) {
    const id = req.params.orderId;

    OrderModel.findByIdAndRemove(id)
        .then(() => res.status(200).json({
            success: true,
        }))
        .catch((err) => res.status(500).json({
            success: false,
        }));
}

module.exports = { createOrder, getAllOrderOfUser, getAllOrder, updateOrder, getSingleOrder, deleteOrder }
