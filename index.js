const express = require("express"); // Tương ứng với import express from 'express'
const mongoose = require("mongoose"); // Tương ứng với import mongoose from 'mongoose'

const voucherRouter = require("./src/routes/VoucherRouter"); 
const drinkRouter = require("./src/routes/DrinkRouter");
const userRouter = require('./src/routes/UserRouter');
const orderRouter = require('./src/routes/OrderRouter');

const app = express();

// Khai báo body lấy tiếng Việt
app.use(express.urlencoded({
    extended: true
}))
//Khai báo body dạng JSON
app.use(express.json());

const port = 8000;

// Kết nối với MongoDB
async function connectMongoDB() {
    await mongoose.connect("mongodb://localhost:27017/pizza365");
}

//Thực thi kết nối
connectMongoDB()
    .then(() => console.log("Connect MongoDB Successfully"))
    .catch(err => console.log(err))

app.get("/", (request, response) => {
    response.json({
        message: "Pizza365 API"
    })
})

app.use("/vouchers", voucherRouter);
app.use('/drinks', drinkRouter);
app.use('/users', userRouter);
app.use('/orders', orderRouter);

app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})